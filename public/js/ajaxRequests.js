// закрытие формы редактирования при загрузке страницы
$(document).ready(function() {
    popUpHide();
});

// получение данных о пользователе из базы, вставка данных в форму
async function popUpShow(id) {
    await getData(id);
    $("#editForm").show();
}

// непосредственно получение данных для заполнения формы редактирования
function getData(id) {
    console.log('in get data');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '/management/getUserData/' + id,
        async: true,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#edit_user").attr('name', data.message.id_user);
            $("#first_name_popup").val(data.message.first_name);
            $("#last_name_popup").val(data.message.last_name);
            $("#email_popup").val(data.message.email);
            $("#company_name_popup").val(data.message.company_name);
            $("#position_popup").val(data.message.position);
            $("#phone_1").val(data.message.phone_1);
            $("#phone_2").val(data.message.phone_2);
            $("#phone_3").val(data.message.phone_3);
            console.log(data.message)
        },
        error: function(error) {
            console.log('шото в гетдате пошло не так');
        }
    });
}

// обновление данных по сотруднику
$('#edit_user').submit(function (e) {
    e.preventDefault();
    var user_id = parseInt($('#edit_user').attr('name'));
    $.ajax({
        type: 'POST',
        url: '/management/edit/' + user_id,
        data: $('#edit_user').serialize(),
        success: function (data) {
            console.log(data.message);
            if (data.type === 'wrong_name') {
                alert(data.message);
            } else if (data.type === 'wrong_email') {
                alert(data.message);
            } else {
                alert("Данные успешно изменены");
                location.reload();
            }
        },
        error: function () {
            console.log('ERRRRR')
        }
    });
});

function popUpHide() {
    $("#editForm").hide();
}
