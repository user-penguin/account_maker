<?php

namespace App\Repository;

use App\Entity\Worker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Worker|null find($id, $lockMode = null, $lockVersion = null)
 * @method Worker|null findOneBy(array $criteria, array $orderBy = null)
 * @method Worker[]    findAll()
 * @method Worker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Worker::class);
    }


    /**
     * запрос на получение воркеров из БД с нужного номера и заданного интервала
     * @param $page
     * @param $size
     * @return Worker[] Returns an array of Worker objects
     */

    public function findWorkersByInterval($page, $size)
    {
        return $this->createQueryBuilder('w')
            ->setFirstResult(($page - 1) * $size)
            ->setMaxResults($size)
            ->getQuery()
            ->getResult();
    }

    /**
     * функция ищет совпадение записей с воркерами по емэйлу
     * @param $email
     * @return mixed
     */
    public function findWorkersByEmail($email) {
        $workers = $this->createQueryBuilder('w')
            ->where('w.email = :uEmail')
            ->setParameter('uEmail', $email)
            ->getQuery()
            ->getResult();

        return $workers;
    }
}
