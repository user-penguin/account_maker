<?php

namespace App\Controller;

use App\Entity\Worker;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ManagementController extends AbstractController
{
    /**
     * редирект с "/" на рабочую страницу
     * отправка на фронт нужного количества работников в зависимости от выбранной страницы пагинации
     * @Route("/", name="main")
     */
    public function main() {
        return $this->redirectToRoute("management", ['id' => 1]);
    }


    /**
     * отправка на фронт нужного количества работников в зависимости от выбранной страницы пагинации
     * @Route("/management/{id}", name="management", requirements={"id"="\d+"})
     * @param Integer $id
     * @return Response
     */
    public function index($id)
    {
        $size = 10;

        $countOfWorkers = count($this->getDoctrine()->getRepository(Worker::class)->findAll());

        $countOfPages = $countOfWorkers / 10;
        if ($countOfWorkers % 10 > 0)
        {
            $countOfPages++;
        }

        // массив с индексами для twig
        $indexes = [];
        for ($i = 1; $i <= $countOfPages; $i++)
        {
            array_push($indexes, $i);
        }


        // находим нужный интервал рабочих с заданным смещением
        $workers = $this
            ->getDoctrine()
            ->getRepository(Worker::class)
            ->findWorkersByInterval($id, $size);


        return $this->render('management/alluser.html.twig', [
            'controller_name' => 'ManagementController',
            'workers' => $workers,
            'current' => $id,
            'indexes' => $indexes,
            'countOfWorkers' => $countOfWorkers,
        ]);
    }

    /**
     * переход на страницу просмотра всех записей, редирект на первую страницу в пагинации
     * @Route("/management", name="management_redirect")
     */
    public function indexRedirect()
    {
        return $this->redirectToRoute("management", ['id' => 1]);
    }

    /**
     * рендер страницы добавления нового пользователя
     * @Route("/management/create", name="create_user")
     */
    public function addUserPage()
    {
        return $this->render('management/create.html.twig');
    }

    /**
     * Добавление нового юзера
     * @Route("/management/add", name="add_user", methods={"POST"})
     */
    public function addNewUser()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $worker = $this->getWorkerFromPost();

        // обработка ошибок, проверка на дублирование email
        try {
            $entityManager->persist($worker);
            $entityManager->flush();
            return $this->render('management/create.html.twig', [
                'success_mess' => $_POST["first_name"].' успешно добавлен'
            ]);
        }
        catch (\Exception $ex) {
            $error_message = '';
            if ($ex->getErrorCode() == 1062)
            {
                $error_message = 'Такой емэйл уже есть!';
            } else
            {
                $error_message = 'Незапланированная ошибка';
            }
            return $this->render('management/create.html.twig', [
                'error_list' => $error_message,
                'first_name' => $worker->getFirstName(),
                'last_name' => $worker->getLastName(),
                'email' => $worker->getEmail(),
                'company_name' => $worker->getCompanyName(),
                'position' => $worker->getPosition(),
                'phone_1' => $worker->getTelephone1(),
                'phone_2' => $worker->getTelephone2(),
                'phone_3' => $worker->getTelephone3(),
            ]);
        }
    }

    /**
     * Удаление записи о рабочем
     * @Route ("/management/delete/{id}", name="delete_by_id", requirements={"id"="\d+"})
     * @param $id
     * @return RedirectResponse
     */
    public function deleteWorker($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryWorkers = $this->getDoctrine()->getRepository(Worker::class);
        $worker = $repositoryWorkers->find($id);
        $entityManager->remove($worker);
        $entityManager->flush();
        return $this->redirectToRoute('management_redirect');
    }

    /**
     * получение полей Worker по посту
     * @return Worker
     */
    private function getWorkerFromPost() {
        $worker = new Worker();

        $worker->setFirstName($_POST["first_name"]);
        $worker->setLastName($_POST["last_name"]);
        $worker->setEmail($_POST["email"]);
        $worker->setCompanyName($_POST["company_name"]);
        $worker->setPosition($_POST["position"]);
        $worker->setTelephone1($_POST["telephone_1"]);
        $worker->setTelephone2($_POST["telephone_2"]);
        $worker->setTelephone3($_POST["telephone_3"]);

        return $worker;
    }
}
