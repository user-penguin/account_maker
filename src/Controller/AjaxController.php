<?php

namespace App\Controller;

use App\Entity\Worker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * получение данных для формы редактирования информации о рабочем
     * @Route("/management/getUserData/{id}", name="ajaxGetUser", requirements={"id"="\d+"})
     * @param $id
     * @return Response
     */
    public function index($id)
    {
        $repositoryWorkers = $this->getDoctrine()->getRepository(Worker::class);
        $worker = $repositoryWorkers->find($id);

        $smallJson = [
            'first_name' => $worker->getFirstName(),
            'last_name' => $worker->getLastName(),
            'email' => $worker->getEmail(),
            'position' => $worker->getPosition(),
            'company_name' => $worker->getCompanyName(),
            'phone_1' => $worker->getTelephone1(),
            'phone_2' => $worker->getTelephone2(),
            'phone_3' => $worker->getTelephone3(),
            'id_user' => $worker->getId()
        ];

        return new JsonResponse(array(
            'status' => 'OK',
            'message' => $smallJson
        ),
        200);
    }

    /**
     * изменение данных о работнике, проверка на пустые поля, повторяющийся емэйл
     * @Route("/management/edit/{id}", name="edit", requirements={"id"="\d+"})
     * @param $id
     * @return JsonResponse
     */
    public function editWorker($id) {
        $workerFromForm = $this->getWorkerFromPost();
        $workerFromDB = $this->getDoctrine()->getRepository(Worker::class)->find($id);

        // если емэйл поменялся и он не повторяется
        if ($workerFromForm->getEmail() != $workerFromDB->getEmail()) {
            if (count($this->getDoctrine()
                    ->getRepository(Worker::class)
                    ->findWorkersByEmail($workerFromForm->getEmail())) == 0
            )
            {
                $workerFromDB->setEmail($workerFromForm->getEmail());
            } else
            {
                return new JsonResponse(array(
                    'message' => 'Повтор емэйла, пожалуйста, замените',
                    'type' => 'wrong_email'),
                    200
                );
            }
        }

        if (!$this->isEmptyString($workerFromForm->getFirstName()) &&
            !$this->isEmptyString($workerFromForm->getLastName()))
        {
            $workerFromDB->setFirstName($workerFromForm->getFirstName());
            $workerFromDB->setLastName($workerFromForm->getLastName());
        } else
        {
            return new JsonResponse(array(
                'message' => 'Недопустимы пустые поля имени или фамилии',
                'type' => 'wrong_name'
                ),

                200
            );
        }

        // сохранение остальных полей работника
        $workerFromDB->setCompanyName($workerFromForm->getCompanyName());
        $workerFromDB->setPosition($workerFromForm->getPosition());
        $workerFromDB->setTelephone1($workerFromForm->getTelephone1());
        $workerFromDB->setTelephone2($workerFromForm->getTelephone2());
        $workerFromDB->setTelephone3($workerFromForm->getTelephone3());

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array(
            'message' => 'all success'
        ),
        200);
    }

    /**
     * получение полей Worker по посту
     * @return Worker
     */
    private function getWorkerFromPost() {
        $worker = new Worker();

        $worker->setFirstName($_POST["first_name"]);
        $worker->setLastName($_POST["last_name"]);
        $worker->setEmail($_POST["email"]);
        $worker->setCompanyName($_POST["company_name"]);
        $worker->setPosition($_POST["position"]);
        $worker->setTelephone1($_POST["telephone_1"]);
        $worker->setTelephone2($_POST["telephone_2"]);
        $worker->setTelephone3($_POST["telephone_3"]);

        return $worker;
    }

    /**
     * проверка на пустую строку
     * @param $string - исходная строка
     * @return bool
     */
    private function isEmptyString ($string) {
        return !(isset($string) && (strlen(trim($string)) > 0));
    }
}
